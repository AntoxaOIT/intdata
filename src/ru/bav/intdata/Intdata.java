package ru.bav.intdata;

import java.io.*;

/**
 * Класс для чтения чисел из файла ,проверки исключений ,записи в файл.
 *
 * @author Barinov group 15OIT18.
 */
public class Intdata {
    public static void main(String[] args) throws IOException {

        String numbers;
        File file = new File("D:\\antoxaoit\\intdatafile.dat");
        try (BufferedWriter error = new BufferedWriter(new FileWriter("D:\\antoxaoit\\error.txt"));
             DataOutputStream write = new DataOutputStream(new FileOutputStream(file)); //Запись в файл.
             BufferedReader reader = new BufferedReader(new FileReader("D:\\antoxaoit\\numbers.txt"))) { //Чтение файла.
            while ((numbers = reader.readLine()) != null) { //проверка всех строк.
                try {
                    write.writeInt(Integer.valueOf(numbers)); //перевод из инт в стринг.
                } catch (NumberFormatException er) { //Исключения
                    error.write(er + "/n");
                }

            }
        }
    }
}