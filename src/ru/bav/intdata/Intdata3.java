package ru.bav.intdata;

import java.io.*;

/**
 * Класс для поиска и записи шестизначных чисел являющихся "Счастливыми билетами"
 *
 * @author Barinov group 15OIT18.
 */
public class Intdata3 {

    public static void main(String[] args) throws IOException {
        try (BufferedWriter writefile = new BufferedWriter(new FileWriter("D:\\antoxaoit\\intdatahappyfile.txt")); //Запись в файл.
             DataInputStream readfile = new DataInputStream(new FileInputStream("D:\\antoxaoit\\int6data.dat"))) { //чтение

            while (readfile.available() > 0) {
                int happy = readfile.readInt();
                if (HappyTricked(happy/1000)==HappyTricked(happy%1000)) {

                    writefile.write(String.valueOf(happy) + "\n"); //перевод из инт в стринг
                }
            }
        }
    }

    /**
     * Метод для нахождения суммы трехзначных чисел .
     *
     * @param happy - трехзначное число.
     * @return сумма цифр трехзначного числа.
     */
    private static int HappyTricked(int happy) {
        return happy%10 + happy/10%10 + happy / 100;

    }
}
