package ru.bav.intdata;

import java.io.*;

/**
 * Класс для поиска и записи чисел от 4х до 6ти знаков в файл.
 *
 * @author Barinov group 15OIT18.
 */
public class Intdata2 {
    public static void main(String[] args) throws IOException {
        try (DataOutputStream writefile = new DataOutputStream(new FileOutputStream("D:\\antoxaoit\\int6data.dat")); //Запись в файл.
             DataInputStream readfile = new DataInputStream(new FileInputStream("D:\\antoxaoit\\intdatafile.dat"))) {
            while (readfile.available() > 0) {
                int six = readfile.readInt();
                if (six < 1000000 && six > 999) {
                    writefile.writeInt(six);
                }
            }
        }
    }

}

